jQuery( function( $ ){
	'use strict';

	var $body = $( 'body' );
	var DISAN = DISAN || {};

	DISAN.ajaxSearch = {
		$icon         : $( '.header__search .icofont-search' ),
		$resultWrapper: $( '.header__search .ajax-search-result' ),
		$searchField  : $( '.header__search .search-field' ),
		lastValue     : '',

		init: function() {
			DISAN.ajaxSearch.clickToHide();
			var timeout = null;
			DISAN.ajaxSearch.$searchField.on( 'change keyup', function() {
				var val              = DISAN.ajaxSearch.$searchField.val();
				if ( timeout ) {
					clearTimeout( timeout );
				}
				timeout = setTimeout( DISAN.ajaxSearch.getSearchResults, 500, val );
			} );
		},

		clickToHide: function() {
			$( '.site' ).on( 'click', function( e ) {
				if ( ! $( e.target ).closest( '.header__search' ).length ) {
					DISAN.ajaxSearch.hideSearhResults();
				}
			} );
		},

		// Check nếu giá trị trước đó có bằng với hiện tại, nếu có thì không gửi request (trong các trường hợp nhấn các nút không phải là chữ)
		isLastValueEqual: function( val ) {
			if ( val === DISAN.ajaxSearch.lastValue && DISAN.ajaxSearch.lastValue !== '' ) {
				return true;
			}
			DISAN.ajaxSearch.lastValue = val;
			return false;
		},

		isEmptyInput: function( val ) {
			if ( ! val ) {
				DISAN.ajaxSearch.hideSearhResults();
				return true;
			}
			return false;
		},

		getSearchResults: function( val ) {
			var isLastValueEqual = DISAN.ajaxSearch.isLastValueEqual( val );
			var isEmptyInput     = DISAN.ajaxSearch.isEmptyInput( val );
			if ( isLastValueEqual || isEmptyInput ) {
				return;
			}
			var data = {
				action: 'ajax_search',
				s: val,
				_wpnonce: DISAN_ADDONS.nonce
			};
			DISAN.ajaxSearch.isLoading();
			$.post( DISAN_ADDONS.ajaxURL, data )
			.done( function( response ) {
				DISAN.ajaxSearch.showSearhResults();
				DISAN.ajaxSearch.isDone();
				DISAN.ajaxSearch.$resultWrapper.html( response.data.output );
			} );
		},

		isLoading: function() {
			DISAN.ajaxSearch.$icon.removeClass( 'icofont-search' ).addClass( 'icofont-ui-rotation icofont-spin');
			DISAN.ajaxSearch.$resultWrapper.addClass( 'is-loading' );
		},
		isDone: function() {
			DISAN.ajaxSearch.$icon.addClass( 'icofont-search' ).removeClass( 'icofont-ui-rotation icofont-spin');
			DISAN.ajaxSearch.$resultWrapper.removeClass( 'is-loading' );
		},
		showSearhResults: function() {
			DISAN.ajaxSearch.$resultWrapper.removeClass( 'is-hidden' );
		},
		hideSearhResults: function() {
			DISAN.ajaxSearch.$resultWrapper.empty().addClass( 'is-hidden' );
		}
	};

	DISAN.formUpload = {
		init: function() {
			$( '.header__upload' ).on( 'click', function() {
				$body.addClass( 'is-upload-visible' );
			} );
		},
		hide: function() {
			$( '.upload__overlay, .upload__close' ).on( 'click', function() {
				$body.removeClass( 'is-upload-visible' );
			} );
		}
	};

	$( document ).ready( function() {
		DISAN.ajaxSearch.init();
		DISAN.formUpload.init();
		DISAN.formUpload.hide();
	} );
} );