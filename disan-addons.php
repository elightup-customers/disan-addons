<?php
/**
 * Plugin Name: Disan Addons
 * Description: Description
 * Plugin URI: http...
 * Author: Author
 * Author URI: http...
 * Version: 1.0
 * License: GPL2 or later
 * License URI: https://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain: Text Domain
 * Domain Path: Domain Path
 * Network: false
 */

defined( 'ABSPATH' ) || exit;

define( 'DISAN_ADDONS_PATH', plugin_dir_path( __FILE__ ) );
define( 'DISAN_ADDONS_URL', plugin_dir_url( __FILE__ ) );
define( 'DISAN_TAXONOMIES', json_encode (
	array (
		'di-san-van-hoa' => 'Di sản văn hóa',
		'tinh-thanh'     => 'Tỉnh thành',
		'dan-toc'        => 'Dân tộc',
		'nien-dai'       => 'Niên đại',
		'loai-du-lieu'   => 'Loại dữ liệu',
		'nguon-du-lieu'  => 'Nguồn dữ liệu',
	)
) );

include 'vendor/autoload.php';

new Disan_Addons\Plugin();
new Disan_Addons\PostTypes();
new Disan_Addons\Taxonomies();
new Disan_Addons\AjaxSearch();
new Disan_Addons\UploadDisan();
new Disan_Addons\MetaBox();
new Disan_Addons\Shortcodes\TermsList();
