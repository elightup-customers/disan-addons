<?php
namespace Disan_Addons;

class MetaBox {

	public function __construct() {
		add_filter( 'rwmb_meta_boxes', [ $this, 'register' ] );
		add_filter( 'rwmb_frontend_post_title', [ $this, 'filter_post_title' ] );
		add_filter( 'rwmb_frontend_post_content', [ $this, 'filter_post_content' ] );
	}

	public function register( $meta_boxes ) {
		$loai_dl_terms = get_terms( [
			'taxonomy' => 'loai-du-lieu',
			'hide_empty' => false,
		] );

		$fields = array(
			array(
				'name' => 'Gallery ảnh',
				'id'   => 'disan_gallery',
				'type' => 'image',
			),
			array(
				'name'       => 'Loại dữ liệu',
				'id'         => 'disan_loai-du-lieu',
				'type'       => 'taxonomy',
				'taxonomy'   => 'loai-du-lieu',
				'field_type' => 'select_tree',
			),
		);
		foreach( $loai_dl_terms as $term ) {
			if ( $term->slug === 'text' ) continue;
			$name = $term->name;
			if ( $term->slug === 'video' ) {
				$name .= ' (.mp4)';
			} else if ( $term->slug === 'audio' ) {
				$name .= ' (.mp3)';
			}
			$loai_du_lieu_args = array(
				'name'             => 'File ' . $name,
				'id'               => 'disan_' . $term->slug,
				'type'             => 'file',
				'max_file_uploads' => 1,
				'visible'          => array( 'disan_loai-du-lieu', '=', $term->term_id ),
			);
			if ( $term->slug !== 'video' && $term->slug !== 'audio' ) {
				$loai_du_lieu_args['upload_dir'] = WP_CONTENT_DIR . '/3D/media/';
			}
			$fields[] = $loai_du_lieu_args;
		}
		$fields[] = array(
		    'type' => 'custom_html',
		    'std'  => '<strong>Lựa chọn chuyên mục upload:</strong>',
		);
		$taxonomies = json_decode( DISAN_TAXONOMIES );

		foreach( $taxonomies as $slug => $name ) {
			if ( in_array( $slug, array( 'dan-toc' ) ) ) {
				$fields[] = array(
					'name'       => $name,
					'id'         => 'disan_' . $slug,
					'type'       => 'taxonomy',
					'taxonomy'   => $slug,
					'field_type' => 'select_advanced',
					'multiple'   => true,
				);
			} elseif ( 'loai-du-lieu' === $slug ) {
				continue;
			} else {
				$fields[] = array(
					'name'       => $name,
					'id'         => 'disan_' . $slug,
					'type'       => 'taxonomy',
					'taxonomy'   => $slug,
					'field_type' => 'select_tree',
				);
			}
		}
		$meta_boxes[] = array(
			'id'         => 'disan_meta_boxes',
			'title'      => 'Thông tin liên quan',
			'post_types' => 'di-san',
			'context'    => 'normal',
			'priority'   => 'high',
			'fields'     => $fields,
		);

		return $meta_boxes;
	}

	public function filter_post_content( $field ) {
		$field['type'] = 'textarea';
		$field['name'] = 'Nội dung';
		return $field;
	}

	public function filter_post_title( $field ) {
		$field['name'] = 'Tiêu đề*';
		$field['required'] = true;
		return $field;
	}
}
