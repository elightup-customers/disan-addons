<?php
namespace Disan_Addons;

class Taxonomies {
	public function __construct() {
		add_action( 'init', array( $this, 'register' ), 0 );
	}

	public function register() {
		$taxonomies = json_decode( DISAN_TAXONOMIES );

		foreach ( $taxonomies as $slug => $name ) {
			$args = array (
				'label' => $name,
				'labels' => array(
					'menu_name'             => $name,
					'all_items'             => esc_html__( 'Tất cả ', 'disan' ) . $name,
					'edit_item'             => esc_html__( 'Sửa ', 'disan' ) . $name,
					'view_item'             => esc_html__( 'Xem ', 'disan' ) . $name,
					'update_item'           => esc_html__( 'Update ', 'disan' ) . $name,
					'add_new_item'          => esc_html__( 'Thêm mới ', 'disan' ) . $name,
					'new_item_name'         => $name . esc_html__( ' mới', 'disan' ),
					'parent_item'           => $name . esc_html__( ' cha', 'disan' ),
					'parent_item_colon'     => $name . esc_html__( ' cha:', 'disan' ),
					'search_items'          => esc_html__( 'Tìm kiếm ', 'disan' ) . $name,
					'add_or_remove_items'   => esc_html__( 'Thêm hoặc xóa ', 'disan' ) . $name,
					'choose_from_most_used' => esc_html__( 'Choose most used ', 'disan' ) . $name,
					'not_found'             => esc_html__( 'Không tìm thấy ', 'disan' ) . $name,
					'name'                  => $name,
					'singular_name'         => $name,
				),
				'public'               => true,
				'show_ui'              => true,
				'show_ui'              => true,
				'meta_box_cb'          => false,
				'show_in_menu'         => true,
				'show_in_nav_menus'    => true,
				'show_tagcloud'        => true,
				'show_in_quick_edit'   => true,
				'show_admin_column'    => false,
				'show_in_rest'         => true,
				'hierarchical'         => true,
				'query_var'            => true,
				'sort'                 => true,
				'rewrite_no_front'     => false,
				'rewrite_hierarchical' => false,
				'rewrite'              => true,
			);

			register_taxonomy( $slug, array( 'di-san' ), $args );
		}
	}
}