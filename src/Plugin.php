<?php
namespace Disan_Addons;

class Plugin {
	public function __construct() {
		add_action( 'wp_enqueue_scripts', [ $this, 'enqueue' ] );
		add_action( 'admin_enqueue_scripts', [ $this, 'admin_enqueue' ] );
		add_filter( 'upload_mimes', [ $this, 'register_myme_types' ] );
	}

	public function enqueue() {
		wp_enqueue_style( 'disan-addon-style', DISAN_ADDONS_URL . 'style.css', array(), '1.0' );
		wp_enqueue_script( 'disan-addon-script', DISAN_ADDONS_URL . 'js/script.js', array( 'jquery' ), '1.0', true );
		wp_localize_script( 'disan-addon-script', 'DISAN_ADDONS', array(
			'ajaxURL' => admin_url( 'admin-ajax.php' ),
			'nonce'   => wp_create_nonce( 'ajax-nonce' ),
		) );
	}

	public function admin_enqueue() {
		wp_enqueue_style( 'disan-addon-admin-style', DISAN_ADDONS_URL . 'admin.css', array(), '1.0' );
	}

	public function register_myme_types( $mime_types ){
		$mime_types['glb'] = 'application/vnd.hzn-3d-crossword';
		return $mime_types;
	}
}