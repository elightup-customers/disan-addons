<?php
namespace Disan_Addons;

class UploadDisan {
	public function __construct() {
		add_action( 'astra_masthead_content', [ $this, 'button' ], 12 );
		// add_action( 'wp_footer', [ $this, 'form_upload' ] );
		add_action( 'rwmb_frontend_after_save_post', [ $this, 'set_post_thumbnail' ] );
		add_action( 'rwmb_frontend_after_save_post', [ $this, 'add_gallery_to_content' ] );
		add_action( 'rwmb_frontend_after_save_post', [ $this, 'add_attachment_to_content' ] );
		add_action( 'rwmb_frontend_after_save_post', [ $this, 'update_disan_uploaded_status' ] );
		add_action( 'publish_di-san', [ $this, 'update_disan_published_status' ], 10, 2 );
		// add_action( 'admin_bar_menu', [ $this, 'admin_bar_notification' ], 99 );
		add_filter( 'openid-connect-generic-login-button-text', [ $this, 'login_button_text' ] );

	}

	public function login_button_text( $text ) {
		return __( 'Đăng nhập với itrithuc.vn', 'disan' );
	}

	public function button() {
		?>
		<div class="header__login">
			<?php
			if ( ! is_user_logged_in() ) {
				echo do_shortcode( '[openid_connect_generic_login_button]' );
			} else {
				$user_id = get_current_user_id();
				$username = get_user_meta( $user_id, 'first_name', true );
				?>
				<div>
					<p><strong>Xin chào, <?php echo $username; ?></strong></p>
					<a href="<?php echo esc_url( wp_logout_url() ); ?>'">Thoát</a>
				</div>
				<?php if ( current_user_can( 'edit_post', get_the_ID() ) ) : ?>
					<a href="<?php echo disan_get_page_phe_duyet(); ?>" class="badge"><?php echo $this->get_new_uploaded_disan(); ?></a>
				<?php endif; ?>
				<a class="header__upload button" href="<?php echo esc_url( site_url( '/upload-di-san' ) ); ?>">Upload</a>
				<?php
			}
			?>
		</div>
		<?php
	}

	public function form_upload() {
		if ( isset( $_GET['rwmb_frontend_field_post_id'] ) ) {
			return;
		}
		$form = '[mb_frontend_form id="disan_meta_boxes" post_status="draft" post_fields="title, content" submit_button="Gửi" confirmation="Cám ơn bạn đã đóng góp"]';
		?>
		<div class="disan-form-upload">
			<div class="upload__overlay"></div>
			<div class="upload__content">
				<div class="upload__close upload__close--top"><i class="icofont-close-line"></i></div>
				<?php echo do_shortcode( $form ); ?>
				<button type="button" class="upload__close upload__close--bottom">Nhấn vào đây để tiếp tục</button>
			</div>
		</div>
		<?php
	}

	/**
	 * Lấy ảnh đầu tiên từ gallery làm thumbnail
	 */
	public function set_post_thumbnail( $object ) {
		$gallery = $this->get_gallery( $object );
		if ( empty ( $gallery ) ) {
			return;
		}
		$image = reset( $gallery );
		set_post_thumbnail( $object->post_id, $image['ID'] );
	}

	/**
	 * Chèn gallery vào content;
	 */
	public function add_gallery_to_content( $object ) {
		$post_id = $object->post_id;
		$gallery = $this->get_gallery( $post_id );
		if ( empty ( $gallery ) ) {
			return;
		}
		// Lọc ra image attachment.
		$gallery = array_filter( $gallery, 'wp_attachment_is_image' );
		$gallery_ids = implode( ', ', array_keys( $gallery ) );
		$shortcode   = '[gallery ids="' . $gallery_ids . '" size="disan_archive"]';

		$this->update_post_content( $post_id, $shortcode );
	}

	public function add_attachment_to_content( $object ) {
		$post_id = $object->post_id;
		$types = [
			'audio'           => 'add_audio_to_content',
			'video'           => 'add_video_to_content',
			'hoat-canh-3d'    => 'add_3d_dong_to_content',
			'mo-hinh-3d-tinh' => 'add_3d_tinh_to_content',
			'mo-hinh-3d-dong' => 'add_3d_dong_to_content',
		];
		foreach( $types as $type => $function ) {
			$file = $this->get_attachment_file( $post_id, $type );
			if ( empty( $file ) ) continue;
			call_user_func_array( [ $this, $function ], array( $post_id, $file ) );
		}
	}

	public function add_video_to_content( $post_id, $file ) {
		if ( ! in_array( $file['mime_type'], [ 'video/mpeg', 'video/x-mpeg', 'video/mp4' ] ) ) {
			return;
		}
		$url       = $file['url'];
		$shortcode = '[video width="1920" height="1024" mp4="' . $url . '"][/video]';
		$this->update_post_content( $post_id, $shortcode );
	}

	public function add_audio_to_content( $post_id, $file ) {
		if ( ! in_array( $file['mime_type'], [ 'audio/mpeg', 'audio/mpeg3', 'audio/x-mpeg-3' ] ) ) {
			return;
		}
		$url       = $file['url'];
		$shortcode = '[audio mp3="' . $url . '"][/audio]';
		$this->update_post_content( $post_id, $shortcode );
	}

	public function add_3d_tinh_to_content( $post_id, $file ) {
		$info = pathinfo( $file['name'] );
		if ( 'glb' !== $info['extension'] ) return;
		$url       = content_url() . '/3D/h1r/embed/?url=../../media/' . $file['name'];
		$shortcode = '<iframe src="' . $url . '" width="100%" height="500px"></iframe>';
		$this->update_post_content( $post_id, $shortcode );
	}

	public function add_3d_dong_to_content( $post_id, $file ) {
		$info = pathinfo( $file['name'] );
		if ( 'glb' !== $info['extension'] ) return;
		$url       = content_url() . '/3D/h3r/embed/?glb=../../media/' . $file['name'];
		$shortcode = '<iframe src="' . $url . '" width="100%" height="500px"></iframe>';
		$this->update_post_content( $post_id, $shortcode );
	}

	// Chèn shortcode từ file upload.
	public function update_post_content( $post_id, $shortcode ) {
		$old_content = get_post_field( 'post_content', $post_id );
		wp_update_post( [
			'ID'           => $post_id,
			'post_content' => $shortcode . '<br/>' . $old_content,
		] );
	}

	/**
	 * Update disan status khi di san được upload.
	 */
	public function update_disan_uploaded_status( $object ) {
		update_post_meta( $object->post_id, 'new_disan_uploaded', true );
	}

	/**
	 * Update disan status khi disan được publish.
	 */
	public function update_disan_published_status( $id, $post ) {
		update_post_meta( $id, 'new_disan_uploaded', false );
	}

	public function get_gallery( $post_id ) {
		return rwmb_meta( 'disan_gallery', [ 'size' => 'disan_archive' ], $post_id );
	}

	public function get_attachment_file( $post_id, $type ) {
		$file = rwmb_meta( 'disan_' . $type, '', $post_id );
		if ( empty( $file ) ) {
			return;
		}
		$file = reset( $file );
		return $file;
	}

	/*public function admin_bar_notification( \WP_Admin_Bar $wp_admin_bar ) {
		$wp_admin_bar->add_node( [
			'id'    => 'di_san_uploaded',
			'title' => '<span class="bubble">' . $this->get_new_uploaded_disan() . '</span> Di sản upload',
			'href'  => admin_url( 'edit.php?post_status=draft&post_type=di-san' ),
		] );
	}*/

	public function get_new_uploaded_disan() {
		$uploaded_disan = new \WP_Query( [
			'post_type'      => 'di-san',
			'posts_per_page' => -1,
			'post_status'    => array( 'draft' ),
			'meta_key'       => 'new_disan_uploaded',
			'meta_value'     => 1,
			'fields'         => 'ids',
		] );
		return $uploaded_disan->found_posts;
	}
}
