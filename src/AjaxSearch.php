<?php
namespace Disan_Addons;

class AjaxSearch {
	public function __construct() {
		add_action( 'astra_masthead_content', [ $this, 'render' ], 11 );
		add_action( "wp_ajax_ajax_search", [ $this, 'get_search_results' ] );
		add_action( "wp_ajax_nopriv_ajax_search", [ $this, 'get_search_results' ] );
	}

	public function render() {
		?>
		<div class="header__search">
			<form role="search" method="get" class="search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
				<label>
					<span class="screen-reader-text"><?php esc_html_e( 'Tìm kiếm', 'disan' ); ?></span>
					<input class="search-field" placeholder="<?php esc_attr_e( 'Tìm kiếm', 'disan' ); ?>" value="<?php the_search_query(); ?>" name="s" type="search">
				</label>
				<button type="submit" class="search-submit"><i class="icofont-search"></i><span class="screen-reader-text">Search</span> </button>
			</form>
			<div class="ajax-search-result is-hidden"></div>
		</div>
		<?php
	}

	public function get_search_results() {
		check_ajax_referer( 'ajax-nonce' );

		if ( empty( $_POST['s'] ) ) {
			return;
		}

		$args = [
			'post_status'    => 'publish',
			'posts_per_page' => 4,
			's'              => sanitize_text_field( $_POST['s'] ),
		];

		$query = new \WP_Query( $args );
		$output = '';
		if ( ! $query->have_posts() ) {
			wp_send_json_success( array(
				'output' => 'Không có kết quả',
			) );
		}
		ob_start();
		?>
		<div class="search-result__list">
			<?php
			while( $query->have_posts() ) {
				$query->the_post();
				?>
				<article <?php post_class( 'ajax-search' ); ?>>
					<div class="entry-thumb">
						<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
							<?php
							if ( has_post_thumbnail() ) {
								the_post_thumbnail( array( 80, 80, true ) );
							} else {
								?>
								<img width="80" height="80" src="<?php echo esc_url( get_stylesheet_directory_uri() . '/images/no-image.jpg' ); ?>" class="wp-post-image" alt="" itemprop="image">
								<?php
							}
							?>
						</a>
					</div>
					<div class="entry-text">
						<div class="entry-title">
							<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
								<?php the_title(); ?>
							</a>
						</div>
					</div>
				</article>
				<?php
			}
			wp_reset_postdata();
			?>
		</div>
		<div class="search-result__more">
			<a href="<?php echo esc_url( site_url( '/?s=' . $_POST['s'] ) ) ?>">Xem thêm các kết quả</a>
		</div>
		<?php
		$output = ob_get_clean();
		wp_send_json_success( array(
			'output' => $output,
		) );
	}
}
