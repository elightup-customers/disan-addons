<?php
namespace Disan_Addons;

class PostTypes {
	public function __construct() {
		add_action( 'init', [ $this, 'register' ], 0 );
	}

	public function register() {
		$args = array (
			'label' => esc_html__( 'Di sản', 'disan' ),
			'labels' => array(
				'menu_name'          => esc_html__( 'Di sản', 'disan' ),
				'name_admin_bar'     => esc_html__( 'Di sản', 'disan' ),
				'add_new'            => esc_html__( 'Thêm mới', 'disan' ),
				'add_new_item'       => esc_html__( 'Thêm mới Di sản', 'disan' ),
				'new_item'           => esc_html__( 'Di sản mới', 'disan' ),
				'edit_item'          => esc_html__( 'Sửa Di sản', 'disan' ),
				'view_item'          => esc_html__( 'Xem Di sản', 'disan' ),
				'update_item'        => esc_html__( 'Update Di sản', 'disan' ),
				'all_items'          => esc_html__( 'Tất cả Di sản', 'disan' ),
				'search_items'       => esc_html__( 'Tìm kiếm Di sản', 'disan' ),
				'not_found'          => esc_html__( 'Không tìm thấy Di sản', 'disan' ),
				'not_found_in_trash' => esc_html__( 'Không tìm thấy Di sản', 'disan' ),
				'name'               => esc_html__( 'Di sản', 'disan' ),
				'singular_name'      => esc_html__( 'Di sản', 'disan' ),
			),
			'public'              => true,
			'exclude_from_search' => false,
			'publicly_queryable'  => true,
			'show_ui'             => true,
			'show_in_nav_menus'   => true,
			'show_in_admin_bar'   => true,
			'show_in_rest'        => false,
			'capability_type'     => 'post',
			'hierarchical'        => false,
			'has_archive'         => true,
			'query_var'           => true,
			'can_export'          => true,
			'rewrite_no_front'    => false,
			'show_in_menu'        => true,
			'show_in_rest'        => true,
			// 'taxonomies'          => array( 'post_tag' ),
			'supports'            => array(
				'title',
				'editor',
				'thumbnail',
				'post-formats',
				'excerpt',
			),
			'rewrite' => true,
		);

		register_post_type( 'di-san', $args );
	}
}
