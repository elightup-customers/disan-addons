<?php
namespace Disan_Addons\Shortcodes;

class TermsList {
	public function __construct() {
		$this->icon = [ 'institution', 'laboratory', 'folder', 'university' ];
		add_shortcode( 'terms_list', [ $this, 'render' ] );
	}

	public function render( $atts ) {
		$atts = shortcode_atts( [
			'title'    => '',
			'taxonomy' => '',
			'type'     => 'list',
		], $atts, 'terms_list' );
		$title    = $atts['title'];
		$type     = $atts['type'];
		$taxonomy = $atts['taxonomy'];
		if ( empty( $taxonomy ) ) return;
		ob_start();
		?>
		<div class="term-list-shortcode term-list-shortcode--<?php echo esc_attr( $type ); ?>">
			<?php
			if ( $type === 'icon' ) {
				$this->render_icon_type( $title, $taxonomy );
			} else {
				$this->render_list_type( $title, $taxonomy );
			}
			?>
		</div>
		<?php
		return ob_get_clean();
	}

	public function get_terms( $taxonomy ) {
		$terms = get_terms( [
			'taxonomy' => $taxonomy
		] );

		return $terms;
	}

	public function render_icon_type( $title, $taxonomy ) {
		$terms = $this->get_terms( $taxonomy );
		// reset index.
		$terms = array_values( $terms );
		if ( empty( $terms ) || is_wp_error( $terms ) ) {
			return;
		}
		$count = $this->sum_all_count( $terms );
		if ( empty ( $count ) ) {
			return;
		}
		?>
		<div class="shortcode__title">
			<h3><?php echo esc_html( $title ); ?></h3>
			<small class="shortcode__disan-count"><?php echo $count . ' di sản'; ?></small>
		</div>
		<div class="shortcode__content">
			<?php foreach( $terms as $index => $term ) :
				$term_link = add_query_arg( array(
				'?fwp_' . $taxonomy => $term->slug,
				), site_url( '/di-san/' ) );
				?>
				<a href="<?php echo esc_url( $term_link ); ?>" class="term-block">
					<div class="block__icon">
						<i class="icofont-<?php echo esc_attr( $this->icon[$index] ); ?>"></i>
					</div>
					<div class="block__content"><span><?php echo esc_html( $term->name ); ?></span></div>
				</a>
			<?php endforeach; ?>
		</div>
		<?php
	}

	public function reorder_terms( $terms, $order ) {
		$slugs = array_map( function( $term ) {
			return $term->slug;
		}, $terms );
		$slug_key_arr = array_combine( $slugs, $terms );
		$ordered_terms = [];
		foreach( $order as $slug ) {
			if ( empty( $slug_key_arr[ $slug ] ) ) continue;
			$ordered_terms[ $slug ] = $slug_key_arr[ $slug ];
		}
		return $ordered_terms;
	}

	public function sum_all_count( $terms ) {
		if ( empty( $terms ) ) return;
		$count_array = wp_list_pluck( $terms, 'count' );
		return array_sum( $count_array );
	}

	public function render_list_type( $title, $taxonomy ) {
		$terms = $this->get_terms( $taxonomy );
		if ( empty( $terms ) || is_wp_error( $terms ) ) {
			return;
		}
		if ( 'loai-du-lieu' === $taxonomy ) {
			$order = [ 'text', 'audio', 'video', 'mo-hinh-3d-tinh', 'mo-hinh-3d-dong', 'hoat-canh-3d' ];
			$terms = $this->reorder_terms( $terms, $order );
		}
		?>
		<div class="shortcode__title">
			<h3><?php echo esc_html( $title ); ?></h3>
		</div>
		<div class="shortcode__content">
			<?php foreach( $terms as $index => $term ) :
				$term_link = add_query_arg( array(
				'?fwp_' . $taxonomy => $term->slug,
				), site_url( '/di-san/' ) );
				?>
				<a href="<?php echo esc_url( $term_link ); ?>" class="term-block">
					<i class="icofont-caret-right"></i>
					<?php echo esc_html( $term->name ); ?> <span class="term__count"><?php echo '(' . esc_html( $term->count ) . ')' ; ?></span>
				</a>
			<?php endforeach; ?>
		</div>
		<?php
	}

}
